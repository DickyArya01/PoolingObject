using System.Collections;
using System.Collections.Generic;
using Services;
using UnityEngine;

public class PoolObjectSpawner : IServices
{
    ObjectPoolManager pool;

    
    public void poolObjectToSpawn(PoolObject type)
    {
        
        if (pool == null)
        {
            pool = Services.ServiceLocator.Current.GetServices("ObjectPoolManager") as ObjectPoolManager;
            pool.Initialize();
        }


        GameObject gameObject = pool.GetPoolObject(type);

        if (gameObject != null && type != PoolObject.Enemy)
        {
            gameObject.transform.position = GameObject.FindWithTag("Player").transform.position + Vector3.right;;
            
            gameObject.SetActive(true);
        } else
        {
            gameObject.transform.position = randomPosition();; 
            gameObject.SetActive(true);
        }
        
    }

    public Vector3 randomPosition()
    {
        float playerX = GameObject.FindWithTag("Player").transform.position.x + Random.Range(7f, 10.0f);    

        Vector3 position = new Vector3(playerX , Random.Range(-4.0f, 4.0f), 0);

        return position;
    }

}
