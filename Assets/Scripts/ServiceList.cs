﻿using UnityEngine;


namespace Services
{
    [CreateAssetMenu(fileName = "Service List", menuName = "STVR/Create Service List")]
    public class ServiceList : ScriptableObject
    {
        public ObjectPoolManager ObjectPoolManagerPrefabs;
        
        public EnemySpawner EnemySpawnerPrefabs;


    }

    public interface IServices
    {

    }
}