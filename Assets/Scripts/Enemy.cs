using System;
using System.Collections;
using System.Collections.Generic;
using Services;
using UnityEngine;

public class Enemy : MonoBehaviour 
{
    private Transform player;

    private Rigidbody2D rb2d;
    private float speed = .6f;

    private float Lives;

    private Vector2 dir;
    private Vector2 movement;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        player = GameObject.FindWithTag("Player").transform;
        rb2d = GetComponent<Rigidbody2D>();
        Lives = 3;
    }

    void FixedUpdate()
    {
       
       dir = player.position - transform.position; 

       dir.Normalize();

       movement = dir;
        Move(movement);        
    }

    private void Move(Vector2 movement)
    {
        rb2d.MovePosition((Vector2) transform.position + (movement * speed * Time.deltaTime));

    }

    void OnCollisionEnter2D(Collision2D other)
    {
       if (other.gameObject.tag == "Bullet")
       {
            Destroy();
       } 
    }

    private void Destroy() 
    {
        if (Lives == 0)
        {
            Destroy(this.gameObject);   
        }
        Lives--;
    }
}
