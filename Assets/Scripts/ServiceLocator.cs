using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Services
{
    public class ServiceLocator
    {
        public static ServiceLocator Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new ServiceLocator();
                }

                return _current;
            }
        }

        private static ServiceLocator _current;

        Dictionary<string, IServices> _services = new Dictionary<string, IServices>();

        public ServiceLocator()
        {
            Initialize();
        }

        private void Initialize()
        {
            var serviceList = Resources.Load<ServiceList>("Data/Service List");

            var ObjectPoolManagerPrefabs = GameObject.Instantiate(serviceList.ObjectPoolManagerPrefabs);
            var playerInput = new PlayerInput();
            var playerAction = new PlayerAction();
            var enemySpawner = GameObject.Instantiate(serviceList.EnemySpawnerPrefabs); 
            var poolObjectSpawner = new PoolObjectSpawner();

            _services.Add("ObjectPoolManager", ObjectPoolManagerPrefabs);
            _services.Add("PlayerInput", playerInput);
            _services.Add("PlayerAction", playerAction);
            _services.Add("EnemySpawner", enemySpawner);
            _services.Add("PoolObjectSpawner", poolObjectSpawner);
        }

        public IServices GetServices(string service)
        {
            return _services[service];
        }
    }
}