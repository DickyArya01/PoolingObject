using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{

    [SerializeField] private float despawnTime;

    ObjectPoolManager pool;

    void Start()
    {
        pool = Services.ServiceLocator.Current.GetServices("ObjectPoolManager") as ObjectPoolManager;
        
    }
    void Update()
    {
       StartCoroutine(Despawn()); 
    }

    private IEnumerator Despawn()
    {
        yield return new WaitForSeconds(despawnTime);
        gameObject.SetActive(false);
        pool.CoolObject(this.gameObject, PoolObject.Wall);
    }
}
