using Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : IServices
{

   PoolObjectSpawner service; 

    public void playerInputHandler(PlayerAction action)
    {
      service = Services.ServiceLocator.Current.GetServices("PoolObjectSpawner") as PoolObjectSpawner;

       if (Input.GetMouseButtonDown(0))
       {
            service.poolObjectToSpawn(PoolObject.Bullet);
       }

       if (Input.GetMouseButtonDown(1))
       {
            service.poolObjectToSpawn(PoolObject.Wall);
          
       }

       if ( Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
       {
            action.playerMovement(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
       } 
    }
}
