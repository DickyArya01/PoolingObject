using System.Collections;
using System.Collections.Generic;
using Services;
using UnityEngine;

public class EnemySpawner : MonoBehaviour, IServices
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private float delaySpawnDuration;
    
    private Transform player;
    
    PoolObjectSpawner spawner;
    float Timer;
    
    void Awake()
    {
        player = GameObject.FindWithTag("Player").transform;    
        Timer = 0f;
    }

    void Start()
    {
        spawner = Services.ServiceLocator.Current.GetServices("PoolObjectSpawner") as PoolObjectSpawner;
        
    }

    void Update()
    {
       SpawnEnemyWithDelay(); 
    }

    void SpawnEnemyWithDelay()
    {
        if (Timer <= delaySpawnDuration)
        {
            Timer += Time.deltaTime;   
        }
        else
        {
            Timer = 0f;
            spawner.poolObjectToSpawn(PoolObject.Enemy);
        }
    }



}
