using Services;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAction : IServices
{
    private float speed = 10f;
    [SerializeField] private Transform player;


    public void playerMovement(float x, float y)
    {
        player.localPosition += player.up * speed * y * Time.deltaTime;
        player.localPosition += player.right * speed * x * Time.deltaTime;
    }

    

    public void Initialize(Transform player)
    {
        this.player = player;
    }

}
