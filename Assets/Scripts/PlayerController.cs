using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    [SerializeField] private Transform player;
    // [SerializeField] private Transform bulletSpawn;
    [SerializeField] private GameObject bulletPrefab;

    PlayerInput input;
    PlayerAction action;


    private void Start()
    {
        action = Services.ServiceLocator.Current.GetServices("PlayerAction") as PlayerAction;
        action.Initialize(player);
    }


    // Update is called once per frame
    void Update()
    {
        input = Services.ServiceLocator.Current.GetServices("PlayerInput") as PlayerInput;
        input.playerInputHandler(action);
    }






}
