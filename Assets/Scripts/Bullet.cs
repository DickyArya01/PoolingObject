using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float speed;

    private new Renderer renderer;

    ObjectPoolManager pool;
    // Start is called before the first frame update
    void Start()
    {
        renderer = GetComponent<Renderer>();
        pool = Services.ServiceLocator.Current.GetServices("ObjectPoolManager") as ObjectPoolManager;
    }

    // Update is called once per frame
    void Update()
    {
       transform.position += Vector3.right * speed; 
       DestroyBullet();
    }

    void DestroyBullet()
    {
        if (!renderer.isVisible)
        {
            gameObject.SetActive(false);
            pool.CoolObject(this.gameObject, PoolObject.Bullet);
            
        }
    }
}
