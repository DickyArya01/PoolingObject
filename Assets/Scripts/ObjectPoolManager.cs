using Services;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviour , IServices
{

    [SerializeField] private List<ObjectPool> listOfPool; 

    [SerializeField] private List<GameObject> pool;  
    public void Initialize()
    {
        CheckObjectType(listOfPool);
    }

    private void CheckObjectType(List<ObjectPool> objectType)
    {
        for (int i = 0; i < objectType.Count; i++)
        {
           FillPool(objectType[i]); 
        }
    }

    private void FillPool(ObjectPool objectType)
    {
        for (int i = 0; i < objectType.amount; i++)
        {
           GameObject poolObjectInstance = null;
           poolObjectInstance = Instantiate(objectType.prefab); 
           poolObjectInstance.transform.parent = objectType.container.transform;
           poolObjectInstance.SetActive(false);
           objectType.pool.Add(poolObjectInstance);
        }
    }

    public GameObject GetPoolObject(PoolObject type)
    {
        ObjectPool selected = GetPoolObjectByType(type);
        pool = selected.pool;
        GameObject gameObject = null;

        if (pool.Count > 0)
        {
            gameObject = pool[pool.Count - 1];
            pool.Remove(gameObject);
        }

        else{
            gameObject = Instantiate(selected.prefab);
            gameObject.transform.parent = selected.container.transform;
        }

        return gameObject;

    }

    public void CoolObject(GameObject go, PoolObject type)
    {
        go.SetActive(false);

        ObjectPool selected = GetPoolObjectByType(type);
        List<GameObject> pool = selected.pool;    

        if (!pool.Contains(go))
            pool.Add(go);
    }


    public ObjectPool GetPoolObjectByType(PoolObject type)
    {
        for (int i = 0; i < listOfPool.Count; i++)
        {
            if (type == listOfPool[i].objectType)
            {
                return listOfPool[i];
            }
        }
        return null;
        
    }

}
